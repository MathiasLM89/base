/********** .env **********/
require('dotenv')
	.config();

/********** Node Modules **********/
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var _ = require('lodash');
var request = require('request');

/********** Project Modules **********/
// var network = require('./project_modules/network');

/********** Actual app **********/
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use('/dependencies', express.static('node_modules'));

app.get('/', function (req, res) {
	res.render('index');
});

// Initialization
app.listen(process.env.PORT, function () {
	console.log('Listening on port ' + process.env.PORT);
});

module.exports = app;
